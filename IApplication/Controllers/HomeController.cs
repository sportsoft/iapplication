﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IApplication.Models;

namespace IApplication.Controllers
{
    class Country
    {
        public string Name { get; set; }
    }

    public class HomeController : Controller
    {
        private IApplicationContext _context;
      
        public HomeController(IApplicationContext context)
        {
            this._context = context;
        }

        public IActionResult Index()
        {
            List<Student> students = _context.Students.ToList();

            return View(students);
        }

        [HttpGet]
        public JsonResult Create(Student student)
        {
            if( ModelState.IsValid )
            {
                _context.Students.Add(student);
                _context.SaveChanges();
            }

            return Json(ModelState);
        }

        [HttpPost]
        public RedirectToActionResult EditSave(Student student)
        {
            if (ModelState.IsValid)
            {
                if (student.Id is null)
                {
                    _context.Students.Add(student);
                }
                else
                {
                    _context.Students.Update(student);
                }
                _context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int StudentId)
        {
            Student model = _context.Students.FirstOrDefault(x => x.Id == StudentId) ?? new Student();

            return View(model);
        }

        [HttpGet]
        public JsonResult Delete(int Id)
        {
            Student student = _context.Students.Where(x => x.Id == Id).FirstOrDefault();

            if(student != null)
            {
                _context.Students.Remove(student);
                _context.SaveChanges();
            }
            
            return Json(ModelState);
        }

        [HttpGet]
        public IActionResult Select(int StudentId)
        {
            // Объяснение - я поставил проверку на Null на случай, если студент еще не существует или уже удален.
            // Вместо этого можно использовать Redirect.
            Student model = _context.Students.FirstOrDefault(x => x.Id == StudentId) ?? new Student();
            
            return View(model);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
