﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IApplication.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using IApplication.Models;

namespace IApplication.Controllers
{
    public class IocController : Controller
    {
        private StudentService _studentService;
        private IApplicationContext _context;

        public IocController(IApplicationContext context, StudentService studentService)
        {
            _context = context;
            _studentService = studentService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult Get(int StudentId)
        {
            return Json(_studentService.Get(StudentId));
        }

        public JsonResult GetFromMethod(int StudentId, [FromServices] StudentService tempService)
        {
            return Json(tempService.Get(StudentId));
        }

        public JsonResult GetFromHTTPContext(int StudentId)
        {
            var tempService = HttpContext.RequestServices.GetService<StudentService>();

            return Json(tempService.Get(StudentId));
        }

        public JsonResult GetFromActivator(int StudentId)
        {
            var tempService = ActivatorUtilities.CreateInstance<StudentService>(HttpContext.RequestServices, _context);

            return Json(tempService.Get(StudentId));
        }


    }
}