﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IApplication.Models.Entities
{
    public class StudentRating
    {
        public Int32 StudentId { get; set; }

        public Int32 LectureId { get; set; }

        [Display(Name = "Оценка")]
        public byte Rating { get; set; }
    }
}
