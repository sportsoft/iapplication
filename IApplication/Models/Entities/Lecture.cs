﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IApplication.Models.Entities
{
    public class Lecture
    {
        public Int32 LectureId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
