﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IApplication.Models
{
    public class Student
    {
        public int? Id { get; set; } = null;

        [Required(ErrorMessage = "Ошибка! Заполните Имя")]
        [StringLength(128)]
        [Display(Name = "Имя")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Ошибка! Заполните Фамилию")]
        [StringLength(128)]
        [Display(Name = "Фамилия")]
        public string Lastname { get; set; }

        [StringLength(128)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [StringLength(128)]
        [Display(Name = "Номер телефона")]
        public string Phone { get; set; }

        [StringLength(128)]
        [Display(Name = "Название института")]
        public string Institute { get; set; }
    }
}
