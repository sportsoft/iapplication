﻿using IApplication.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IApplication.Models
{
    public class IApplicationContext : DbContext
    {
        public DbSet<Lecture> Lectures { get; set; }

        public DbSet<Student> Students { get; set; }

        public DbSet<StudentRating> StudentRating { get; set; }

        public IApplicationContext(DbContextOptions<IApplicationContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Составной ключ
            modelBuilder.Entity<StudentRating>().HasKey(u => new { u.StudentId, u.LectureId });
        }
    }
}
