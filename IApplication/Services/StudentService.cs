﻿using IApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IApplication.Services
{
    public interface IStudentService
    {
        bool Delete(int StudentId);

        Student Get(int StudentId);
    }

    public class StudentService : IStudentService
    {
        private IApplicationContext _context;

        public StudentService(IApplicationContext context)
        {
            _context = context;
        }

        public bool Delete(int StudentId)
        {
            Student student = _context.Students.Where(x => x.Id == StudentId).FirstOrDefault();

            if (student != null)
            {
                _context.Students.Remove(student);
                _context.SaveChanges();

                return true;
            }

            return false;
        }

        public Student Get(int StudentId)
        {
            return _context.Students.FirstOrDefault(x => x.Id == StudentId);
        }
    }
}
