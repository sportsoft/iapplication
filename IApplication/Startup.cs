﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using System.Text;
using Microsoft.EntityFrameworkCore;
using IApplication.Models;
using IApplication.Services;

namespace IApplication
{
    public class Startup
    {
        private string contentRootPath = "";

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            contentRootPath = env.ContentRootPath;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");

            // Relative path for database file
            if (connection.Contains("%CONTENTROOTPATH%"))
            {
                connection = connection.Replace("%CONTENTROOTPATH%", contentRootPath);
            }

            services.AddDbContext<IApplicationContext>(options =>
                options.UseSqlServer(connection));

            services.AddTransient<StudentService>();

            services.AddMvc();
        }
       
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // MyMiddleware data don't write to Static files, therefore UseStaticFiles before MyMiddleware
            app.UseStaticFiles();

            // Middleware in Class
            // app.UseMyMiddleware();

            // Or
            // app.UseMiddleware<MyMiddleware>();

            // Simple middleware
            //app.Use((context, next) =>
            //{
            //    DateTime dateStart = DateTime.Now;

            //    next.Invoke();

            //    TimeSpan interval = DateTime.Now - dateStart;
            //    string dateOut = "[" + interval.ToString(@"%s\.FFFFF") + " sec] ";

            //    context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(dateOut), 0, dateOut.Length);

            //    return Task.Delay(0);
            //}); 

            // Others
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }

    public class MyMiddleware
    {
        private readonly RequestDelegate _next;

        public MyMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            DateTime dateStart = DateTime.Now;

            await _next.Invoke(context);

            TimeSpan interval = DateTime.Now - dateStart;
            string dateOut = "[Second Middleware execute time " + interval.ToString(@"%s\.FFFFF") + " sec] ";

            await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(dateOut), 0, dateOut.Length);
        }
    }

    public static class MyExtensions
    {
        public static IApplicationBuilder UseMyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MyMiddleware>();
        }
    }
}